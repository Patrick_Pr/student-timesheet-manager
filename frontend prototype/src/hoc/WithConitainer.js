import { contains } from "jquery";

import React from "react";

const container = (props) => (
	<div className={props.classes} style={props.style}>{props.children}</div>
);

export default container;