import React, { Component, Fragment } from "react";
import "./App.css";

import NavBar from "../Components/NavigationBar/NavBar";
import InputContainer from "../Components/Input/InputContainer";
import SelectorContainer from "../Components/Selector/SelectorContainer";
import TableContainer from "../Components/MonthTable/TableContainer";
import axios from "../axios"

/**
 * This is the container for the main page of the timesheet manager app.
 * And this container component is managing the state object.
 */
class App extends Component {
	constructor(props) {
		super(props);
		const currDate = new Date(Date.now());
		const min =
			currDate.getMinutes() < 10
				? "0" + currDate.getMinutes()
				: currDate.getMinutes();

		const hour =
			currDate.getHours() < 10
				? "0" + currDate.getHours()
				: currDate.getHours();

		const currTime = hour + ":" + min;

		this.state = {
			checkInState: false,
			selMonth: currDate.getMonth(),
			selYear: currDate.getFullYear(),
			checkInDate: "",
			time: currTime,
			checkInDateValid: true,
			timeData: {
				days: []
			}
		};
	}

	async componentDidMount(){
		try{
			const response = await axios.get("/data");
			console.log(response.data);
			this.setState({timeData: response.data})
		}catch (err) {
			console.error(err);
		} 
	}

	validateInput(time) {
		let rgx = "[0-9][0-9]:[0-9][0-9]";

		this.setState({
			checkInDateValid: time.length !== time.match(rgx).length,
		});
		console.log(
			this.state.checkInDateValid,
			time.match(rgx),
			time.length !== time.match(rgx).length
		);
	}

	checkInHandler = (event) => {
		if (
			event.key === "Enter" ||
			event.key === null ||
			event.key === undefined
		) {
			this.validateInput(this.state.time);
			if(this.state.checkInDateValid){
				this.setState({
					checkInState: true,
				});
			}
		}
	};

	checkOutHandler = (event) => {
		if (
			event.key === "Enter" ||
			event.key === null ||
			event.key === undefined
		) {
			this.validateInput(this.state.time);
			if(this.state.checkInDateValid){
				this.setState({
					checkInState: false,
				});
			}
		}
	};

	restetStates = () => {
		this.setState({
			checkInState: false,
		});
	};

	updateInputTimeHandler = (event) => {
		console.log(event.target.value);

		this.setState({
			time: event.target.value,
		});
	};

	updateMonthHandler = (event) => {
		console.log("before", this.state.selMonth);
		this.setState({
			selMonth: parseInt(event.target.value),
		});
		console.log("after", parseInt(event.target.value));
	};

	updateYearHandler = (event) => {
		console.log("before", this.state.selYear);
		this.setState({
			selYear: parseInt(event.target.value),
		});
		console.log("after", parseInt(event.target.value));
	};

	render() {
		return (
			<Fragment>
				<header>
					<NavBar title="Timesheet Manager" />
				</header>
				<main className="container-fluid m-2">
					<InputContainer
						checkInState={this.state.checkInState}
						checkIn={this.checkInHandler}
						checkOut={this.checkOutHandler}
						changeHandler={this.updateInputTimeHandler}
						curentTime={this.state.time}
						valid={this.state.checkInDateValid}
					/>

					<SelectorContainer
						changeYear={this.updateYearHandler}
						monthNum={this.state.selMonth}
						yearNum={this.state.selYear}
						changeMonth={this.updateMonthHandler}
					/>

					<TableContainer
						monthNum={this.state.selMonth}
						yearNum={this.state.selYear}
						data={this.state.timeData}
					/>
				</main>

				<footer>
					<nav className="navbar navbar-light bg-light">Placeholder</nav>
				</footer>
			</Fragment>

			// <div>
			// 	<nav className="navbar navbar-dark bg-dark">
			// 		<h1 className="navbar-brand mb-1 h1">Timesheet Manager</h1>
			// 	</nav>

			// 	<div className="container">
			// 		<div
			// 			className="shadow"
			// 			style={{ border: "1px solid #ccc", marginTop: "1px" }}>
			// 			<div className="row m-5">
			// 				<CheckIn status={this.state.checkedIn} checkInHandler={this.checkIn}/>
			// 				<CheckOut status={this.state.checkedOut} checkOutHandler={this.checkOut}/>
			// 			</div>
			// 		</div>

			// 		<div
			// 			className="shadow mt-3 mb-3"
			// 			style={{ border: "1px solid #ccc", marginTop: "1px" }}>
			// 			<div className="row m-5">
			// 				<MonthSelector />
			// 				<YearSelector />
			// 			</div>

			// 			<div
			// 				className="row m-5 shadow p-3"
			// 				style={{ border: "1px solid #ccc", marginTop: "1px" }}>
			// 				<button type="button" className="btn btn-outline-primary  mb-2">
			// 					Export to Excel
			// 				</button>
			// 				<MonthTable />
			// 			</div>
			// 		</div>
			// 	</div>

			// 	<nav className="navbar navbar-light bg-light">Placeholder</nav>
			// </div>
		);
	}
}

export default App;
