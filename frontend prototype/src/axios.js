import axios from "axios"

const instace = axios.create({
  baseURL: "http://localhost:8080/timesheetapp"
});

instace.defaults.headers.common["Content-Type"] = "application/json";

export default instace;