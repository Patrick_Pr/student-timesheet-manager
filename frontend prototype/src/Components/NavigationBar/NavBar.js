import React from "react";

const navBar = (props) => (
	<nav className="navbar navbar-dark bg-dark">
		<h1 className="navbar-brand mb-1 h1">{props.title}</h1>
	</nav>
);

export default navBar;
