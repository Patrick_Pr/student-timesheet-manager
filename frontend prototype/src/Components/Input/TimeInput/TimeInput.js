import React, { Fragment } from "react";

import CheckButton from "../CheckButton/CheckButton";

const timeInput = (props) => {
	let minutes = props.minutes < 10 ? "0" + props.minutes : props.minutes;
	let hours = props.hour < 10 ? "0" + props.hour : props.hour;

	let inputClasses = "form-control";

	if (!props.valid) {
		inputClasses.concat(" is-invalid");
	}
	return (
		<div className="col d-flex justify-content-center">
			<div className="form-row">
				<div className="col-auto mt-2 mb-2 text-center">
					<div className="input-group mt-2 mb-2">
						<input
							id="timeInput"
							type="text"
							className={inputClasses}
							placeholder={hours + ":" + minutes}
							value={props.curentTime}
							onChange={props.changeHandler}
							onKeyPress={props.checkHandler}
						/>
						<span
							className="input-group-append input-group-text"
							id="basic-addon2"
						>
							Uhr
						</span>	
					</div>
					<CheckButton
						type={props.type}
						checkHandler={props.checkHandler}
						onKey
					/>
				</div>
			</div>
		</div>
	);
};

export default timeInput;
