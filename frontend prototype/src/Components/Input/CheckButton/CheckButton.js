import React from "react";

const checkButton = (props) => {
  const text = props.type;
  let classes;
  if(props.type === "Check In"){
    classes = "btn btn-success btn-block";
  }else if(props.type === "Check Out"){
    classes = "btn btn-danger btn-block";
  }

	return (
		<button
			type="button"
			style={{height: "60px"}}
			className={classes}
			onClick={props.checkHandler}>
			{text}
		</button>
	);
};

export default checkButton;
