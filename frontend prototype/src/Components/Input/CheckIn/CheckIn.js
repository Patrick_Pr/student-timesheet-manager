import React from "react";

import Container from "../../../hoc/WithConitainer";

const checkIn = (props) => {
	const badge = (
		<div className="badge badge-secondary" style={{ fontSize: "larger" }}>
			Checked in 20:20
		</div>
	);
	return (
		<button
			type="button"
			className="btn btn-success btn-lg m-2"
			onClick={props.checkInHandler}>
			Check In
		</button>
	);
};

export default checkIn;
