import React from "react";

import CheckIn from "./CheckIn/CheckIn";
import CheckOut from "./CheckOut/CheckOut";
import TimeInput from "./TimeInput/TimeInput";

import Container from "../../hoc/WithConitainer";

const inputContainer = (props) => {
	const curDate = new Date(Date.now());

	let buttonProps;

	if (props.checkInState) {
		buttonProps = {
			type: "Check Out",
			handler: props.checkOut,
		};
	} else {
		buttonProps = {
			type: "Check In",
			handler: props.checkIn,
		};
	}

	// if (props.checkInState) {
	// 	inputField = (
	// 		<div className="row m-5 justify-content-md-center">
	// 			<div className="col col-sm ">
	// 				<div className="form-group mx-sm-3 mb-2">
	// 					<TimeInput
	// 						hour={curDate.getHours()}
	// 						minutes={curDate.getMinutes()}/>
	// 					<CheckButton type="Check Out" checkHandler={props.checkOut} />
	// 					{/* <CheckOut
	// 					checkHandler={props.checkOut}
	// 				/> */}
	// 				</div>
	// 			</div>
	// 		</div>
	// 	);
	// } else {
	// 	inputField = (
	// 		<div className="row m-5 justify-content-md-center">
	// 			<div className="col col-sm m2 ">
	// 				{/* d-flex justify-content-center */}
	// 				<div className="input-group">
	// 					<TimeInput
	// 						hour={curDate.getHours()}
	// 						minutes={curDate.getMinutes()}
	// 					/>
	// 					<CheckButton type="Check In" checkHandler={props.checkIn} />
	// 					{/* <CheckIn checkHandler={props.checkIn} /> */}
	// 				</div>
	// 			</div>
	// 		</div>
	// 	);
	// }

	return (
		<Container classes="container">
			<div className="shadow rounded p-3">
					<TimeInput
						hour={curDate.getHours()}
						minutes={curDate.getMinutes()}
						type={buttonProps.type}
						checkHandler={buttonProps.handler}
						changeHandler={props.changeHandler}
						curentTime={props.curentTime}
						valid={props.valid}
					/>
				</div>
		</Container>
	);
};

export default inputContainer;
