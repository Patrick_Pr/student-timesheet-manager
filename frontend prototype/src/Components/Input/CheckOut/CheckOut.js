import React, { Fragment } from "react";
import Conatiner from "../../../hoc/WithConitainer";

/**
 * This component will display the checkOut Button which is used to get the end time of the work day.
 * @param {Object} props
 */
const checkOut = (props) => {
	const badge = (
		<div className="badge badge-secondary" style={{ fontSize: "larger" }}>
			Checked Out 20:20
		</div>
	);
	return (
		<button
			type="button"
			className="btn btn-danger btn-lg m-2"
			onClick={props.checkOutHandler}>
			Check Out
		</button>
	);
};

export default checkOut;
