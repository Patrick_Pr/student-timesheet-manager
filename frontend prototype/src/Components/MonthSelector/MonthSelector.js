import React from "react";

/**
 * this component will display a selector which can be used to select a month to be displayed in the table.
 * @param {Object} props 
 */
const monthSelector = (props) => {
  return (
    <div className="col">
      <label htmlFor="month">Month:</label>
      <select id="month" className="form-control">
        <option>July</option>
      </select>
    </div>
  )
}

export default monthSelector;