import React from "react";

const yearSelector = (props) => {
	const years = [];
	let startingYear = new Date(Date.now()).getFullYear()-5;

	for (let i = 0; i < 10; i++) {
		years.push(
			<option key={i} value={startingYear}>
				{startingYear}
			</option>
		);
		startingYear++;
	}

	return (
		<div className="col">
			<label htmlFor="year">Year:</label>
			<select
				id="year"
				className="form-control"
				onChange={props.changeYear}
				value={props.yearNum}>
				{years}
			</select>
		</div>
	);
};

export default yearSelector;
