import React from "react";

import MonthSelector from "./MonthSelector/MonthSelector";
import YearSelector from "./YearSelector/YearSelector";

import Container from "../../hoc//WithConitainer";

const selectorContainer = (props) => {
  console.log("selectorContainer", props.monthNum);
	return (
		<Container classes="container">
			<div className="shadow rounded">
				<div className="row mt-5 mb-5 p-3">
					<div className="col-sm">
						<MonthSelector
							changeMonth={props.changeMonth}
							monthNum={props.monthNum}
						/>
					</div>
					<div className="col-sm">
						<YearSelector
							changeYear={props.changeYear}
							yearNum={props.yearNum}
						/>
					</div>
				</div>
			</div>
		</Container>
	);
};

export default selectorContainer;
