import React from "react";

const monthSelector = (props) => {
	const months = [];
	const monthStrings = [
		"Januar",
		"Februar",
		"März",
		"April",
		"Mai",
		"Juni",
		"July",
		"August",
		"September",
		"Oktober",
		"November",
		"Dezember",
	];
  
	for (let i = 0; i < 13; i++) {
		months.push(
			<option key={i} value={i}>
				{monthStrings[i]}
			</option>
		);
	}

	return (
		<div className="col">
			<label htmlFor="month">Month:</label>
			<select onChange={props.changeMonth} value={props.monthNum}  id="month" className="form-control">
				{months}
			</select>
		</div>
	);
};
export default monthSelector;
