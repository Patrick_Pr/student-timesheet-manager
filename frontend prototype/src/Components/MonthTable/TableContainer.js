import React from "react";

import MonthTable from "./MonthTable";

import Container from "../../hoc/WithConitainer";

const tableContainer = (props) => {
	return (
		<Container classes="container">
			<div className="shadow rounded">
				<div className="row mt-5 pl-3 pt-3 pr-3">
					<div className="col-sm">
						<button type="button" className="btn btn-outline-primary  mb-2">
							Export to Excel
						</button>
					</div>
				</div>
				<div className="row mb-5 p-3">
					<div className="col-sm overflow-auto">
						<MonthTable monthNum={props.monthNum} yearNum={props.yearNum} data={props.data}/>
					</div>
				</div>
			</div>
		</Container>
	);
};

export default tableContainer;
