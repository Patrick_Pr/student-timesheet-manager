import React, { Fragment } from "react"

import Day from "../Day/Day"

const week = (props) => {
  
  return (
    <Fragment>
      <Day day="Monday" checkedInTime="10:10" checkedOutTime="15:10" pause="30" total="5:30"/>
      <Day day="Dienstag" checkedInTime="10:10" checkedOutTime="15:10" pause="30" total="5:30"/>
      <Day day="Mittwoch" checkedInTime="10:10" checkedOutTime="15:10" pause="30" total="5:30"/>
      <Day day="Freitag" checkedInTime="10:10" checkedOutTime="15:10" pause="30" total="5:30"/>
      <Day day="Samstag" checkedInTime="10:10" checkedOutTime="15:10" pause="30" total="5:30"/>
      <Day day="Sonntag" checkedInTime="10:10" checkedOutTime="15:10" pause="30" total="5:30"/>
    </Fragment>
  )
}

export default week;