import React from "react";

import Day from "./Day/Day";
import { data } from "jquery";

/**
 * This component will display a table with all days and an time information for a month
 * @param {Object} props
 */
const monthTable = (props) => {
	// const date = new Date(2020,0, 0);
	// console.log(date.getDate())
	const firstDay = new Date(props.yearNum, props.monthNum, 1);
	const numberOfDays = new Date(props.yearNum, props.monthNum + 1, 0).getDate();

	const currDate = new Date(Date.now);

	const dayData = props.data.days;

	const dayStrings = [
		"Montag",
		"Dienstag",
		"Mittwoch",
		"Donnerstag",
		"Freitag",
		"Samstag",
		"Sonntag",
	];

	let days = [];
	let dayCnt = firstDay.getDay() - 1;
	for (let i = 1; i <= numberOfDays; i++) {
		if (dayCnt === 7) dayCnt = 0;
		let found = false;
		let dayString = dayStrings[dayCnt];

		// let day = new Day();
		for (let dataObj of dayData) {
			const startDate = new Date(dataObj.startTimeTimestamp);
			const stopDate = new Date(dataObj.stopTimeTimestamp);
			if (i === dataObj.dayNr && startDate.getMonth() === props.monthNum && startDate.getFullYear() === props.yearNum) {
				found = true;
				
				days.push(
					<Day
						key={i}
						dayNr={i}
						day={dayString}
						checkedInTime={constructTimeString(startDate)+" h"}
						checkedOutTime={constructTimeString(stopDate)+" h"}
						pause={dataObj.pauseMin+" min"}
						total={calculateWorkTime(startDate, stopDate, dataObj.pauseMin)}
					/>
				);
			}
		}
		if (!found) {
			days.push(
				<Day
					key={i}
					dayNr={i}
					day={dayString}
					checkedInTime=""
					checkedOutTime=""
					pause=""
					total=""
				/>
			);
		}
		dayCnt++;
	}

	return (
		<table className="table table-hover">
			<thead className="thead-dark">
				<tr>
					<th scope="col">Day</th>
					<th scope="col">Checked In</th>
					<th scope="col">Checked Out</th>
					<th scope="col">Pause</th>
					<th scope="col">Total</th>
				</tr>
			</thead>
			<tbody>{days}</tbody>
		</table>
	);
};

/**
 * 
 * @param {Date} date The date object from wich the time string shoud be constructed.
 */
const constructTimeString = (date) => {
	const min = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();

	const hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();

	return hour.toString() + ":" + min.toString()
};

/**
 * 
 * @param {Date} startTime 
 * @param {Date} stopTime 
 * @param {number} pause 
 */
const calculateWorkTime = (startTime, stopTime, pause) => {
	let startHour = startTime.getHours();
	let startMin = startTime.getMinutes();

	let stopHour = stopTime.getHours();
	let stopMin = stopTime.getMinutes();

	let start = (startHour * 60) + startMin;
	let stop = (stopHour * 60) + stopMin;

	let worktime = stop - start - pause;
	return parseInt(worktime / 60) + ":" + (worktime%60)+ " h";
}
export default monthTable;
