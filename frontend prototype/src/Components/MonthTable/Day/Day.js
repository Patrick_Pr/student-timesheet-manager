import React from "react";

/**
 * This component will be display day in a table row
 * @param {Object} props 
 */
const day = (props) => {
	let dayRow = [];

	if(props.day === "Samstag" || props.day === "Sonntag"){
		dayRow = (
			<th className="table-secondary" colSpan="5" scope="row">{props.dayNr+"\t|\t"+props.day}</th>
		)
	}else {
		dayRow.push(<th scope="row" key="1.5">{props.dayNr+"\t|\t"+props.day}</th>);
		dayRow.push(<td key="2">{props.checkedInTime}</td>);
		dayRow.push(<td key="3">{props.checkedOutTime}</td>);
		dayRow.push(<td key="4">{props.pause}</td>);
		dayRow.push(<td key="5">{props.total}</td>);
	}

	return (
		<tr>
			{dayRow}
		</tr>
	);
};

export default day;
