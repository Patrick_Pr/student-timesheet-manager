import React from "react";

/**
 * this component will display a selector which can be used to select a year to be displayed in the table.
 * @param {Object} props 
 */
const yearSelector = (props) => {
  return (
    <div className="col">
      <label htmlFor="year">Year:</label>
      <select id="year" className="form-control">
        <option>2020</option>
      </select>
    </div>
  )
}

export default yearSelector;