# Student Timesheet manager
## Table of Contens
1. [Introduction](#id-introduction)
2. [Interface](#id-interface)
3. [Planed Usage](#id-planed-usage)
4. [Tech Stack](#id-tech-stack)
5. [Updates](#id-updates)

## Introduction 
<div id="id-introduction"></div>

#### Please consider that this tool is in a very early sate and the user interface, planned usage/Features and the tech stack can change at any time. Feedback about all the topics is welcome.

This project is aimed to ease the work with timesheets.
It can be very convoluted to work with excel timesheets as time management tool.
From my experience working with execl timesheets can be frustrating especialy when the sheets contain several scripts which are slowing down the speed of excel.

At this point I got the idea to develop a tool where you can manage your working hours and only generate an excel sheet if you have to send a report.


<div id="id-interface"></div>

## Interface

![Example Interface picture](./docs/pictures/ExampleInterfacePicture.png)


<div id="id-planed-usage"></div>

## Planed Usage 
Following I wil describe some of the core planed use cases.

#### 1 The check in/out
The check in/out procedure contains two phases first the **check in** second the **check out**. for every step to first card displayed on the interface will change it's apereance. Following this card will be called ***check in/out***

When the site is open for the first time the ***check in/out*** card wil diplayed as followed.  
![CkecInInput](./docs/pictures/CheckInInput.png)
![CkecInInput](./docs/pictures/CheckOutInput.png)

The field which contains the time ![TimeInput](./docs/pictures/TimeInput.png) will be used to input the time which one started/ended his workday.  

With the ![CkecInBtn](./docs/pictures/CheckInButton.png) button or the **Return** key one can register the input in the above mentioned ipnput field to check in.  

And with the ![CkecOutBtn](./docs/pictures/CheckOutButton.png) button or the **Return** key one can register the input in the above mentioned ipnput field to check out. 

For both cases **check in** and **check out** the app will take the time from the **input** and enter it into the row for the current day. The **check in** time will be set in the Checked In column and the **check out** time will be set in the Checked Out column.

#### 2 The Selectin on a month and a year
![Month Day Selector](./docs/pictures/Mont_YearSelector.png)  
These two fields let one select a previus Month for the case it is whished to be reviewed or another excel sheet should be generated. The table will automatically update the dispayed days according to the selected month and year.

The ***Export to Excel*** button will let you save an generated excel file to your local machine.
![Table](./docs/pictures/ExportButton_Table.png)


<div id="id-tech-stack"></div>

## Tech Stack

The user inteface of this Application is written using the [ReactJS library](https://reactjs.org/) and the CSS Framework [Bootstrap](https://getbootstrap.com/).  

And the Backend will be written either in [NodeJS](https://nodejs.org/en/) with the [Express framework](https://expressjs.com/) or in Java using the [Spring Boot framework](https://spring.io/projects/spring-boot)

<div id="id-updates"></div>

## Updates 


### 25.07.2020
* **The following discribed use cases are only implemented on the interface side.**
  * **Furthermore the table contents are placeholder at the momentent and therefore will not change. However the Table will update the displayed days if the month or the year from the month/year selector will be changed.**
  * **The CheckIn time was recently implemented and will update itself on every site reload to the current system time. Please note that all changes made will be los**