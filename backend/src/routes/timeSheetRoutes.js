const express = require("express");

const router = express.Router();

const timesheetController = require("../controller/timesheetController");

router.get("/data", timesheetController.getData);

router.post("/data", timesheetController.setYear);


exports.router = router;