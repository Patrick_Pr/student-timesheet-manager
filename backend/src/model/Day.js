exports.default =  class Day {
  
  /**
   * @param {number} numberInMonth
   * @param {string} dayName 
   * @param {number} startTimeTimestamp 
   * @param {number} stopTimeTimestamp 
   * @param {number} pauseMin an Integer between 0 and 60, otherwhise an errow will be thrown.
   */
  constructor(numberInMonth, dayName, startTimeTimestamp, stopTimeTimestamp, pauseMin){
    this.numberInMonth = numberInMonth
    this.dayName = dayName;
    this.startTimeTimestamp = startTimeTimestamp;
    this.stopTimeTimestamp = stopTimeTimestamp;
    if(pauseMin > 60 || pauseMin < 0) throw new Error("Pause time is not valid. Pause has to be greater than 0 and less than 60!!");
    this.pauseMin = pauseMin;
  }
}