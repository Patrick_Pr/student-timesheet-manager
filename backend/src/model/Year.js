// import Month from "./Month"
const { default: Month } = require("../model/Month");

exports.default =  class Year {

  /**
   * 
   * @param {number} numRep 
   * @param {Month[]} months 
   */
  constructor(numRep, months) {
    this.numRep = numRep;
    this.months = months;
  }
}