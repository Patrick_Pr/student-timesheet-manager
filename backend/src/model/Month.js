// import Day from "./Day"
const { default: Day } = require("../model/Day");

exports.default =  class Month {
  
  /**
   * 
   * @param {string} name 
   * @param {number} numberInYear 
   * @param {Day[]} days 
   */
  constructor(name, numberInYear, days) {
    this.name = name;
    this.numberInYear = numberInYear;
    this.days = days
  }
}