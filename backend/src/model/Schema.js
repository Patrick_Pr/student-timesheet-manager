const mongoose = require("mongoose");

const { default: Year} = require("./Year");

const Schema = mongoose.Schema;

const yearSchema = new Schema();

yearSchema.loadClass(Year);

module.exports = mongoose.model("Year", yearSchema);