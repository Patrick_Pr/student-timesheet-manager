
/**
 * 
 * @param {import("express").Request} req 
 * @param {import("express").Response} res 
 * @param {import("express").NextFunction} next 
 */
exports.siteNotFound = (req, res, next) => {
  console.log(req.body)
  res.status(404).json("Site not found")
}