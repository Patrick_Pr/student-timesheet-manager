const timeSheetService = require("../service/timeSheetService");


/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @param {import("express").NextFunction} next
 */
exports.getData = async (req, res, next) => {
  console.log(req.headers)
  if(req.headers.origin === "http://localhost:3000"){
    res.setHeader("Access-Control-Allow-Origin", req.headers.origin)
    try {
      const dataResponse = await timeSheetService.dataService();
      console.log(dataResponse.data)
      res.status(200).json(dataResponse.data);
    } catch (err) {
      console.log(err)
    }
  } else {
    res.status(400).json({reason: "orgin is not whitelisted!"});
  }
};

/**
 *
 * @param {import("express").Request} req
 * @param {import("express").Response} res
 * @param {import("express").NextFunction} next
 */
exports.setYear = (req, res, next) => {
  try{
    const year = timeSheetService.setUpYear(2020);
    timeSheetService.saveDatabase()
    console.log(year)
    res.status(200).json(year);
  }catch(err) {
    res.status(500).json(err);
  }
}
