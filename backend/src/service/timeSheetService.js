const fs = require("fs");
const path = require("path");
const { default: Axios } = require("axios");
const { response } = require("express");
const YearModel = require("../model/Schema")
const { default: Year} = require("../model/Year");
const { default: Day } = require("../model/Day");
const { default: Month } = require("../model/Month");

const MONTHS = ["Januar", "Februar", "März", "April", "Mai", "Juni", "July", "August", "September", "Oktober", "November", "Dezember"];
const DAYS = ["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"]

const dataPath = path.join(
  path.dirname(process.mainModule.filename),
  "src",
  "data",
  "exampleData.json"
)

const dataURL = "https://timesheet-manager-41353.firebaseio.com/Month.json"

/**
 * @returns {object} parsed JSON data.
 */
exports.dataService = () => {
  console.log(fs.existsSync(dataPath))
  return Axios.get(dataURL)
  // .then(response => {
  //   console.log(response.data)
  //   return response.data;
  // })
  // .catch(error => {
  //   console.error(error);
  // })
}

/**
 * 
 * @param {Year} year 
 */
exports.saveDatabase = (year) => {
  const yearModel = new YearModel({...year});
  yearModel.save()
    .then(result => {
      console.log("created Product");
      console.log(result.collection);
    })
    .catch(err => {
      console.error(err);
    })
}
/**
 * 
 * @param {Number} yearNum
 * @returns {Year} the generated year with the full month set.
 */
exports.setUpYear = yearNum => {
  const monthList = [];

  for(monthNum = 0; monthNum < 12; monthNum++) {
    monthList.push(this.setUpMonth(yearNum, monthNum))
  }
  return new Year(yearNum, monthList)
}

/**
 * @param {number} yearNum
 * @param {number} yearNum
 * @returns {Month} the generated month with day objects set the time stamps in the day objects are filled with null.
 */
exports.setUpMonth = (yearNum, monthNum) => {
  const dayList = [];

  // month + 1 because if the day argument is 0 the Date is created as the last day of the month from the last month.
  // new Date(2020, 0, 0) is the 31th december 2019 
  const upperBorder = new Date(yearNum, monthNum + 1, 0).getDate();

  for(dayNumber = 1; dayNumber <= upperBorder; dayNumber++) {
    let currDay = new Date(yearNum, monthNum, dayNumber)
    dayList.push(new Day(dayNumber, DAYS[currDay.getDay()], null, null, null))
  }
  return new Month(MONTHS[monthNum], monthNum, dayList)
}