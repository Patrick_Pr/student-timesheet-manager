const express = require("express");
const timeSheetRoutes = require("./src/routes/timeSheetRoutes");
const errorController = require("./src/controller/errorController");

const mongoose = require("mongoose");

const timeSheetApp = express();

timeSheetApp.use(express.json({type: "application/json"}));
// timeSheetApp.use(express.urlencoded({extended: true, type: "text/html"}));
timeSheetApp.use("/timesheetapp", timeSheetRoutes.router);
timeSheetApp.use(errorController.siteNotFound);

mongoose.connect("mongodb://localhost:27017/timeSheetNodeJSTests", { useNewUrlParser: true, useUnifiedTopology: true })
  .then(result => {
    timeSheetApp.listen(8080, "localhost");
    console.log("Successfully started timesheet app and connectet to timeSheetNodeJSTests Database");    
  })
  .catch(err => {
    console.log(err);
  })

